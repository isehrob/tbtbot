import json

import tornado.web
import tornado.httpclient

from tbtbot.lib.bot import bot


# Custom `RequestHandler` class which encapsulates the
# common functionality for telegram requests
class RequestHandler(tornado.web.RequestHandler):

    def initialize(self,):
        self.message = None
        self.update = None
        self.chat_id = None
        self.bot = bot
        self.db = None

        if self.request.body:
            self.update = json.loads(self.request.body.decode())
            self.chat_id = self.update['message']['from']['id']

    def prepare(self):
    	self.bot.send_typing(self.chat_id)

    def on_finish(self):
        if self.db:
            # TODO (sehrob): think of how to handle transaction errors
            self.db.commit()
            self.db.close()
