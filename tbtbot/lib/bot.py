import tornado.web
import tornado.httpclient

from tbtbot.lib.telegrambotapi import Message
from tbtbot.lib.configuration import config


class Bot:
    def __init__(self, api):
        self.api = api
        self.client = tornado.httpclient.HTTPClient()

    def send_typing(self, chat_id):
        return self.client.fetch(self.api % 'sendChatAction?chat_id=%s&action=%s'
            % (chat_id, "typing"))

    def send_message(self, chat_id, msg):
        return self.client.fetch(self.api % 'sendMessage?chat_id=%s&text=%s' % (chat_id, msg))

    def send_help(self, chat_id):
        return self.send_message(chat_id, 'What?')


bot = Bot(config.API)
