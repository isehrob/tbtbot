from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from tbtbot.lib.base_model import Base
from tbtbot.lib.configuration import config


class NotImplemented(Exception):
	pass

# database engine which we will use everywhere
def get_engine():
	if config.DB_ENGINE is 'sqlite':
		return create_engine('sqlite:///%s.db' % config.DB_NAME)
	raise NotImplemented('Enginge is not implemented yet!')


def get_db_session():
	# A sessionmaker() instance establishes all conversations with the database
	# and represents a "staging zone" for all the objects loaded into the
	# database session object. Any change made against the objects in the
	# session won't be persisted into the database until you call
	# session.commit(). If you're not happy about the changes, you can
	# revert all of them back to the last commit by calling
	# session.rollback()
	engine = get_engine()
	return sessionmaker(bind=engine)()


def create_all():
	import importlib
	# for some reason that I don't understand yet
	# at the time of table creation all models must be in the scope
	# Ok, gonna learn sqlalchemy later
	importlib.import_module(config.MODELS_MODULE)

	# Base is a declarative base
	# which has all info about user created models
	# and here we create them all with this information
	engine = get_engine()
	Base.metadata.create_all(engine)
