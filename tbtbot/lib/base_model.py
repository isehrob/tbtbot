from sqlalchemy.ext.declarative import declared_attr, declarative_base
from sqlalchemy import Column, Integer


# http://docs.sqlalchemy.org/en/latest/orm/extensions/declarative/mixins.html#augmenting-the-base
class Base(object):
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()
    # __table_args__ = {'mysql_engine': 'InnoDB'}

    id =  Column(Integer, primary_key=True)


# declarative base for our declarative models
# it knows everything about our models so use it
# when you need metadata about models created here
Base = declarative_base(cls=Base)
