import os
import importlib

class ConfigurationNotFound(Exception):
	pass


class ImproperlyConfigured(Exception):
	pass


class ConfigDescriptor:
	def __get__(self, instance, owner):
		if instance._config is None:
			instance._load_from_env()
		return instance._config

# Class of lazy configuration object
class Configuration:

	def __init__(self):
		self._config = None

	CONFIG = ConfigDescriptor()

	def _load_from_env(self):
		try:
			self._config = importlib.import_module(
				os.environ.get('BOT_CONFIG', None))
		except ImportError:
			raise ConfigurationNotFound
		except:
			raise ImproperlyConfigured

config_obj = Configuration()

config = config_obj.CONFIG

def get_config():
	print('CONIG', config)
	return config
