from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String

from tbtbot.lib.base_model import Base


class User(Base):
	user_id = Column(Integer, nullable=False)
	username = Column(String(250))


class Update(Base):
	lastoffset = Column(Integer)