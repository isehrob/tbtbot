import tornado.httpserver
import tornado.ioloop

from tbtbot.lib import Updater
from tbtbot.lib.configuration import config

from app import make_app


def start_bot():
    def callback():
        updater = Updater(
            config.API,
            'http://localhost:%s' % config.SERVER_PORT
        )
        updater.getUpdates(5)

    pcb = tornado.ioloop.PeriodicCallback(callback, config.POLL_INTERVAL)
    pcb.start()

    http_server = tornado.httpserver.HTTPServer(make_app())
    http_server.listen(config.SERVER_PORT, config.SERVER_HOST)

    print(
        'Started the bot at %s:%s' % (
        config.SERVER_HOST,
        config.SERVER_PORT)
    )

    tornado.ioloop.IOLoop.current().start()


def stop_bot():
    tornado.ioloop.IOLoop.current().stop()
    return True
