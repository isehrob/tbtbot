
webhook_env_template = """
# put here your bot token
BOT_TOKEN=<BOT_TOKEN>

# self-signed ssl cert files
CERTFILE=%s
KEYFILE=%s

# tornado server params
SERVER_PORT=8443
SERVER_HOST=0.0.0.0
"""

webhook_config_template = """
\"\"\"tbtbot configuration lives here
\"\"\"

from envparse import env


# reading env
env.read_envfile('.env')

# our bot token
BOT_TOKEN = env('BOT_TOKEN')

# use like this: API % method_name
API = "https://api.telegram.org/bot%s/%s" % (BOT_TOKEN, "%s")

CERTFILE = env('CERTFILE')
KEYFILE = env("KEYFILE")

SERVER_PORT = env('SERVER_PORT')
SERVER_HOST = env('SERVER_HOST')

APP_MODULE = 'server'

MODELS_MODULE = 'models'

ROUTE_MODULE = 'routes'

WEBHOOK_URL = None

DB_ENGINE = 'sqlite'

DB_NAME = 'newbot_db'
"""

update_env_template = """
# put here your bot token
BOT_TOKEN=<BOT_TOKEN>

# tornado server params
SERVER_PORT=8585
SERVER_HOST=0.0.0.0
"""

update_config_template = """
\"\"\"tbtbot configuration lives here
\"\"\"

from envparse import env


# reading env
env.read_envfile('.env')

# our bot token
BOT_TOKEN = env('BOT_TOKEN')

# use like this: API % method_name
API = "https://api.telegram.org/bot%s/%s" % (BOT_TOKEN, "%s")

SERVER_PORT = env('SERVER_PORT')
SERVER_HOST = env('SERVER_HOST')

# in milliseconds
POLL_INTERVAL = 2000

APP_MODULE = 'server'

MODELS_MODULE = 'models'

ROUTE_MODULE = 'routes'

DB_ENGINE = 'sqlite'

DB_NAME = 'newbot_db'

"""

bot_script_file = """#!/usr/bin/env python
import os
import sys


if __name__ == '__main__':
    # inserting current bot path to the PYTHONPATH so
    # the configuration module could be importable
    BOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.insert(0, BOT_DIR)

    # exporting configuration to the environment
    # so the library modules could import it
    os.environ.setdefault('BOT_CONFIG', '%s.configuration')

    from tbtbot.tbtboter.cli import main
    main()

"""
