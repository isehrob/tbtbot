import json

import tornado
import tornado.web

import tbtbot.lib.custom

import routes



def make_app():
	app = tbtbot.lib.Application(
		routes.get_routes(),
		debug=True,
		autoreload=True
	)

	return app
