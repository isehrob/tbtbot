"""Bot routes
"""
from tbtbot.lib.configuration import config

import handlers


def get_routes():
	return [
	    (r"/", handlers.MainHandler, dict(api=config.API), 'command description'),
	    (r"/.*", handlers.UnknownCommandHandler, dict(api=config.API), 'command description')
	]
