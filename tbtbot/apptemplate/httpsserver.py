import tornado.httpserver
import tornado.ioloop
import tornado.web

from tbtbot.lib.configuration import config

from app import make_app


def start_bot():
    app = make_app()
	app.add_handlers('', [
		(r"/webhook", tornado.web.RequestHandler),
	])

    http_server = tornado.httpserver.HTTPServer(
        make_app(),
        ssl_options={
            "certfile": config.CERTFILE,
            "keyfile": config.KEYFILE
        }
    )

    http_server.listen(
    	config.SERVER_PORT,
    	config.SERVER_HOST
    )

    print(
        'Started the bot at %s:%s' % (
        config.SERVER_HOST,
        config.SERVER_PORT)
    )

    tornado.ioloop.IOLoop.current().start()


def stop_bot():
    tornado.ioloop.IOLoop.current().stop()
    return True
