"""Request handlers
"""
from tbtbot.lib import RequestHandler


class MainHandler(RequestHandler):
    # if through getUpdates then the request with GET
    def get(self):
        self.message = "Hey, I'm a new bot and waiting for instructions!"
        self.bot.send_message(self.chat_id, self.message)

    # if through getUpdates then the request with POST
    def post(self):
        self.message = "Hey, I'm a new bot and waiting for instructions!"
        self.bot.send_message(self.chat_id, self.message)


class UnknownCommandHandler(RequestHandler):
    # if through getUpdates then the request with GET
    def get(self):
        self.bot.send_help(self.chat_id)

    # if through getUpdates then the request with POST
    def post(self):
        self.bot.send_help(self.chat_id)
