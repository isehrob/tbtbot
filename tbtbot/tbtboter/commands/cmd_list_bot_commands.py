import click

from tbtbot.lib.configuration import config


@click.command('list_bot_commands', short_help='Lists available commands set for your bot')
def cli():
	"""Lists available commands set for your bot"""
	def remove_slash(cmd):
		return cmd[1:]

	routes = __import__(config.ROUTE_MODULE)
	click.echo('Available commands for your bot')
	for entry in routes.get_routes():
		try:
			cmd, __, __, desc = entry
		except ValueError:
			cmd = entry[0]
			desc = ''
		cmd = remove_slash(cmd)
		click.echo(click.style('%s - %s' % (cmd, desc), fg='green'))
