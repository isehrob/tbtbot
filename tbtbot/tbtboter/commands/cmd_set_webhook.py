import click

from tbtbot.lib.configuration import config


@click.command('set_webhook', short_help='Sets a webhook for your bot')
def cli():
	"""Sets a webhook for your bot"""
	click.echo('setting webhook for', config.API)
	files = {
		'certificate': ('certificate', open(config.CERTFILE, 'rb')),
	}
	data = {'url': config.WEBHOOK_URL}
	url = config.API % 'setWebhook'
	rp = requests.post(url, files=files, data=data)
	click.echo('result of setWebhook: ', rp.status_code, rp.text, rp.reason)
