import os
import shutil

import click

import tbtbot
from tbtbot.tbtboter.commands import cmd_create_ssl_cert
from tbtbot.apptemplate import template_strings as tstrings
from tbtbot.lib.utils import compile_template


# these constants should be self-explanatory
COMMON_TEMPLATE_FILES = [
	'models.py',
	'routes.py',
	'handlers.py',
	'app.py'
]

TEMPLATE_DIR = os.path.join(os.path.dirname(tbtbot.__file__), 'apptemplate')


# helper functions
def set_env():
	shutil.copy('.env.example', '.env')


def copy_common_templates(tmpfiles):
	"""copies common template files into current directory"""
	for filename in tmpfiles:
		src_file = os.path.join(TEMPLATE_DIR, filename)
		tgt_file = os.path.join(os.getcwd(), filename)
		shutil.copy(src_file, tgt_file)


def render_template(template, target, kwargs={}):
	src_file = os.path.join(TEMPLATE_DIR, template)
	new_templ_lines = compile_template(
		open(src_file),
		kwargs
	)
	with open(target, 'w') as f:
		f.writelines(new_templ_lines)


def create_with_webhook(bot_name, certificate_path):
	# creates the telegram bot skeleton suited
	# to get updates by webHook
	cert_path = os.path.abspath(certificate_path)

	copy_common_templates(COMMON_TEMPLATE_FILES)
	# copying app file
	render_template('httpsserver.py', 'server.py')
	render_template('webhook.config.py-tpl', 'configuration.py')
	render_template('bot.py-tpl', 'bot.py', {'botname': bot_name})

	env_template_kwargs = {'private_key': '<your_bots_private.key>',
						   'public_key': '<your_bots_public.key>'}
	if certificate_path:
		env_template_kwargs['private_key'] = os.path.join(
			cert_path, bot_name + '_private.key')
		env_template_kwargs['public_key'] = os.path.join(
			cert_path, bot_name + '_public.key')

	render_template('.env.webhook-tpl', '.env.example', env_template_kwargs)


def create_with_getUpdates(bot_name):
	# creates the telegram bot skeleton without
	# webhook functionality for some reasons
	# for example when the host hasn't static ip
	copy_common_templates(COMMON_TEMPLATE_FILES)
	# copying app file
	render_template('httpserver.py', 'server.py')
	# creating configuration and .env.example files
	render_template('getupdates.config.py-tpl', 'configuration.py')
	render_template('bot.py-tpl', 'bot.py', {'botname': bot_name})
	render_template('.env.getupdates-tpl', '.env.example')


@click.command('create_bot', short_help='Create grand new bot in cwd')
@click.argument('bot_name')
@click.option(
	'--update_type',
	prompt="Ok, how do you want to get your updates?\nwebhooks[1]/long polling[2]",
	type=click.Choice(['1', '2']))
def cli(bot_name, update_type):
	"""Creates grand new bot in cwd"""
	os.mkdir(bot_name)
	os.chdir(os.path.abspath(bot_name))

	if update_type == '1':
		click.echo('creating with webhook')
		cert_path = False
		if click.confirm('Want to create self-signed certificate?'):
			# TODO: is this right?
			cert_path = cmd_create_ssl_cert.create_certificate(bot_name)
			if cert_path is False:
				click.echo(click.style('Couldn\'t create certificate.\
					Creating bot without ssl cert', fg='yellow'))
		create_with_webhook(bot_name, cert_path)

	if update_type == '2':
		click.echo('creating with getUpdates')
		create_with_getUpdates(bot_name)

	click.echo(click.style('DONE!', fg='green'))
