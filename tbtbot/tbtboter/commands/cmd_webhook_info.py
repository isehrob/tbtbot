import click
import tornado

from tbtbot.lib.configuration import config


@click.command('webhook_info', short_help='Displays current webhook set for your bot if any')
def cli():
	"""Displays current webhook set for your bot if any"""
	click.echo('getting webhookinfo for', config.API)
	rp = tornado.httpclient.HTTPClient().fetch(config.API % 'getWebhookInfo')
	click.echo(rp.body)
