import click
import tornado

from tbtbot.lib.configuration import config


@click.command('drop_webhook', short_help='Deletes current webhook if one exist')
def cli():
	"""Starts bot server"""
	click.echo('Deleting webhook', config.API)
	rp = tornado.httpclient.HTTPClient().fetch(config.API % 'deleteWebhook')
	click.echo('result of deleteWebhook\n ', rp.body)
