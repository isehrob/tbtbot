import click

from tbtbot.lib import db


@click.command('sync_db', short_help='Syncs a db for your bot creating missing tables')
def cli():
	"""Syncs a db for your bot creating missing tables"""
	try:
		db.create_all()
	except Exception as e:
		error_ms = '%s\n%s' % ('Couln\'t create db tables!', e)
		raise Exception(click.style(error_ms, fg='red'))
	return True
