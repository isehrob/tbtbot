import importlib
import click

from tbtbot.lib.configuration import config


@click.command('start', short_help='Starts bot server')
def cli():
	"""Starts bot server"""
	app = importlib.import_module(config.APP_MODULE)
	click.echo('Starting the bot...')
	try:
		app.start_bot()
	except KeyboardInterrupt:
		click.echo('stopping the bot...')
		app.stop_bot()
		click.echo("BYE!")
