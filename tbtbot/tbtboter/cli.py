# got from: https://github.com/pallets/click/blob/master/examples/complex/complex/cli.py
import os
import sys
import click


CONTEXT_SETTINGS = dict(auto_envvar_prefix='TBTBOT')

# Context is no more needed
# but it is here for future purposes
class Context():

    def __init__(self, *args, **kwargs):
        self.verbose = False
        self.home = os.getcwd()
        self.config = None

pass_context = click.make_pass_decorator(Context, ensure=True)
cmd_folder = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                          'commands'))


class ComplexCLI(click.MultiCommand):

    def list_commands(self, ctx):
        rv = []
        for filename in os.listdir(cmd_folder):
            if filename.endswith('.py') and \
               filename.startswith('cmd_'):
                rv.append(filename[4:-3])
        rv.sort()
        return rv

    def get_command(self, ctx, name):
        try:
            if sys.version_info[0] == 2:
                name = name.encode('ascii', 'replace')
            mod = __import__('tbtbot.tbtboter.commands.cmd_' + name, None, None, ['cli'])
        except ImportError as e:
            raise e
        return mod.cli


@click.command(cls=ComplexCLI, context_settings=CONTEXT_SETTINGS)
def main():
    """tbtbot commandline interface."""
    pass
